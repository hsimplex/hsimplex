module QuadraticProgramming
(
  QuadraticResult (QSolve, QUnboundedObjectiveFunction),
  quadratic,
  endQuadratic
)
where

import Data.Maybe
import Data.List as List
import qualified Data.Set as Set
import qualified Data.Map as Map
import Numeric.LinearAlgebra as LA
import Auxiliary

data QuadraticResult = QSolve (Vector Double) [Int] | QUnboundedObjectiveFunction

quadratic :: Matrix Double -> Vector Double -> Vector Double -> Matrix Double -> Vector Double -> [Int] -> QuadraticResult
quadratic a b c d x jBasic =
  let aBasic = fromColumns $ map (toColumns a !!) jBasic
      aBasicInv = inv aBasic
      cx = c + d <> x
      cxBasic = LA.fromList $ map (cx @>) jBasic
      u = mapVector negate $ cxBasic <> aBasicInv
      delta = u <> a + cx
      jNonBasic = [j | j <- [0..(cols a - 1)], not $ Set.member j $ Set.fromList jBasic]
      jNonBasic' = filter (\j -> delta @> j ?< 0 || (delta @> j ?> 0 && x @> j ?> 0)) jNonBasic
      j0 = head $ if length jNonBasic' > 1 then tail jNonBasic' else jNonBasic'
      lj0Neg = signum $ delta @> j0
      lBasic = mapVector (* lj0Neg) (aBasicInv <> (toColumns a !! j0))
      jBasicMap = Map.fromList $ zip jBasic [0..]
      l = LA.fromList [if j == j0
                       then negate lj0Neg
                       else if Map.member j jBasicMap
                       then lBasic @> (jBasicMap Map.! j)
                       else 0 | j <- [0..(cols a - 1)]]
      theta = [if j == j0
               then if lj0Neg ?<= 0
                    then Nothing
                    else Just ((x @> j) / lj0Neg)
               else if Map.member j jBasicMap
               then if (l @> j) ?>= 0
                    then Nothing
                    else Just (-(x @> j) / (l @> j))
               else Nothing | j <- [0..(cols a - 1)]]
      thetaValues = catMaybes theta
      theta0 = minimum thetaValues
      ldl = (l <> d) <.> l
      thetaSigma = if ldl ?== 0 then Nothing else Just (abs (delta @> j0) / ldl)
      thetaStar | isNothing thetaSigma = theta0
                | null thetaValues = fromJust thetaSigma
                | otherwise = min theta0 (fromJust thetaSigma)
      jStar = fromJust $ elemIndex (Just theta0) theta
      xNew = x + mulVector l thetaStar
      jBasicNew = replaceSingle jStar j0 jBasic
  in if null jNonBasic'
     then QSolve x jBasic
     else if isNothing thetaSigma && null thetaValues
     then QUnboundedObjectiveFunction
     else if Just thetaStar == thetaSigma || Just thetaStar == theta !! j0
     then quadratic a b c d xNew jBasic
     else quadratic a b c d xNew jBasicNew

hessian :: Matrix Double -> Matrix Double -> Matrix Double
hessian d a =
  buildMatrix (rows d + rows a) (cols d + rows a) (\(r, c) ->
    if r < rows d && c < cols d
    then d @@> (r, c)
    else if r >= rows d && c < cols d
    then a @@> (r - rows d, c)
    else if r < rows d && c >= cols d
    then trans a @@> (r, c - cols d)
    else 0)

endQuadratic :: Matrix Double -> Vector Double -> Vector Double -> Matrix Double -> Vector Double -> [Int] -> [Int] -> QuadraticResult
endQuadratic a b c d x jBasic jBasis =
  let aBasic = fromColumns $ map (toColumns a !!) jBasic
      aBasicInv = inv aBasic
      cx = c + d <> x
      cxBasic = LA.fromList $ map (cx @>) jBasic
      u = mapVector negate $ cxBasic <> aBasicInv
      delta = u <> a + cx
      jNonBasis = [j | j <- [0..(cols a - 1)], not $ Set.member j $ Set.fromList jBasis]
      jNonBasis' = filter (\j -> delta @> j ?< 0) jNonBasis
      j0 = head jNonBasis'
      dStar' = fromRows $ map (toRows d !!) jBasis
      dStar = fromColumns $ map (toColumns dStar' !!) jBasis
      aBasis = fromColumns $ map (toColumns a !!) jBasis
      lBasis = fromList $ take (length jBasis) $ toList $ inv (hessian dStar aBasis) <> mapVector negate (join [toColumns dStar' !! j0, toColumns a !! j0])
      jBasisMap = Map.fromList $ zip jBasis [0..]
      l = LA.fromList [if j == j0
                       then 1
                       else if Map.member j jBasisMap
                       then lBasis @> (jBasisMap Map.! j)
                       else 0 | j <- [0..(cols a - 1)]]
      theta = map (\j -> if l @> j ?>= 0
                         then Nothing
                         else Just (-x @> j / l @> j)) jBasis
      thetaValues = catMaybes theta
      theta0 = minimum thetaValues
      ldl = (l <> d) <.> l
      thetaSigma = if ldl ?== 0 then Nothing else Just (abs (delta @> j0) / ldl)
      thetaStar | isNothing thetaSigma = theta0
                | null thetaValues = fromJust thetaSigma
                | otherwise = min theta0 (fromJust thetaSigma)
      jStar = jBasis !! fromJust (elemIndex (Just theta0) theta)
      xNew = x + mulVector l thetaStar
      jPlus = filter (\j -> elem j jBasis && notElem j jBasic) $ LA.find (?\= 0) $ (toRows aBasicInv !! fromJust (elemIndex jStar jBasic)) <> a
  in if null jNonBasis'
     then QSolve x jBasic
     else if isNothing thetaSigma && null thetaValues
     then QUnboundedObjectiveFunction
     else if Just thetaStar == thetaSigma
     then endQuadratic a b c d xNew jBasic (j0 : jBasis)
     else if not $ elem jStar jBasic
     then endQuadratic a b c d xNew jBasic (removeSingle jStar jBasis)
     else if not $ null jPlus
     then endQuadratic a b c d xNew (replaceSingle jStar (head jPlus) jBasic) (removeSingle jStar jBasis)
     else endQuadratic a b c d xNew (replaceSingle jStar j0 jBasic) (replaceSingle jStar j0 jBasis)
