module Auxiliary where

import Data.Maybe
import Data.List
import Numeric.LinearAlgebra as LA

(?==) :: Double -> Double -> Bool
x ?== y = abs (x - y) < 0.000000001
infix 4 ?==

(?\=) :: Double -> Double -> Bool
x ?\= y = not (x ?== y)
infix 4 ?\=

(?>) :: Double -> Double -> Bool
x ?> y = (x > y) && (x ?\= y)
infix 4 ?>

(?<) :: Double -> Double -> Bool
x ?< y = (x < y) && (x ?\= y)
infix 4 ?<

(?>=) :: Double -> Double -> Bool
x ?>= y = (x > y) || (x ?== y)
infix 4 ?>=

(?<=) :: Double -> Double -> Bool
x ?<= y = (x < y) || (x ?== y)
infix 4 ?<=

mulVector :: Vector Double -> Double -> Vector Double
mulVector a b = mapVector (*b) a

e :: Int -> [Double]
e 0 = 1 : repeat 0
e x = 0 : e (x - 1)

removeByIndex :: Int -> [a] -> [a]
removeByIndex _ [] = error "removeByIndex: index too large."
removeByIndex 0 (_:t) = t
removeByIndex n (h:t) = h : removeByIndex (n - 1) t

removeByIndices :: [Int] -> [a] -> [a]
removeByIndices inds list = removeByIndices' list 0
    where removeByIndices' [] _ = []
          removeByIndices' (h:t) ind | ind `elem` inds = removeByIndices' t (ind + 1)
                                     | otherwise = h : removeByIndices' t (ind + 1)

replaceSingle :: Eq a => a -> a -> [a] -> [a]
replaceSingle _ _ [] = []
replaceSingle x y (h:t) | h == x = y : t
                        | otherwise = h : replaceSingle x y t

removeSingle :: Eq a => a -> [a] -> [a]
removeSingle _ [] = []
removeSingle x (h:t) | h == x = t
                     | otherwise = h : removeSingle x t

removeTwo :: Eq a => a -> a -> [a] -> [a]
removeTwo _ _ [] = []
removeTwo x y (h:t) | h == x = removeSingle y t
                    | h == y = removeSingle x t
                    | otherwise = h : removeTwo x y t

uniqueAlt :: Eq a => [a] -> [a]
uniqueAlt list = uniqueAlt' list list where
        uniqueAlt' [] _ = []
        uniqueAlt' (h:t) orig | isTwo h orig = uniqueAlt' t orig
                              | otherwise = h : uniqueAlt' t orig
        isTwo _ [] = False
        isTwo el (h:t) | el == h = el `elem` t
                       | otherwise = isTwo el t

split12 :: [a] -> ([a], [a])
split12 [] = ([], [])
split12 [x] = ([x], [])
split12 (x:y:xys) = (x:xs, y:ys) where (xs, ys) = split12 xys

isInteger :: Double -> Bool
isInteger x = fromIntegral (round x :: Int) - x ?== 0

fractional :: Double -> Double
fractional x = if isInteger x then 0 else x - fromIntegral (floor x :: Int)

compareInv :: Ord a => a -> a -> Ordering
compareInv a b | a < b = GT
               | a > b = LT
               | otherwise = EQ

maximumIndex :: Ord a => [a] -> Int
maximumIndex list = fromJust $ elemIndex (maximum list) list
