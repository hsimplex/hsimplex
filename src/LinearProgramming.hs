module LinearProgramming
(
  SimplexResult (SSolve, SUnboundedObjectiveFunction),
  FirstPhaseSimplexResult (FPSSolve, FPSNothing, FPSUnboundedObjectiveFunction),
  DualSimplexResult (DSSolve, DSInvalidBasis, DSNothing),
  DualSimplexModResult (DSMSolve, DSMNothing),
  simplex,
  firstPhaseSimplex,
  dualSimplex,
  dualSimplexMod
)
where

import Data.Maybe
import Data.List as List
import qualified Data.Set as Set
import qualified Data.Map as Map
import Numeric.LinearAlgebra as LA
import Auxiliary

data SimplexResult = SSolve (Vector Double) [Int] | SUnboundedObjectiveFunction
data FirstPhaseSimplexResult = FPSSolve (Vector Double) [Int] | FPSNothing | FPSUnboundedObjectiveFunction
data DualSimplexResult = DSSolve (Vector Double) [Int] (Matrix Double) | DSInvalidBasis | DSNothing
data DualSimplexModResult = DSMSolve (Vector Double) [Int] | DSMNothing

firstPhaseSimplex :: Matrix Double -> Vector Double -> Vector Double -> FirstPhaseSimplexResult
firstPhaseSimplex a b c =
  let cFirst' 0 0 = []
      cFirst' 0 y = -1 : cFirst' 0 (y - 1)
      cFirst' x y = 0 : cFirst' (x - 1) y
      cFirst = fromList $ cFirst' (cols a) (rows a)
      aFirst = buildMatrix (rows a) (cols a + rows a) (\(x, y) -> if y < cols a then a @@> (x, y) else if y - cols a == x then 1 else 0)
      SSolve xFirst jBasis = simplex aFirst b cFirst [(cols a)..(cols a + rows a - 1)]
      --aBasis = fromColumns $ map (toColumns aFirst !! j) jBasis
      aBasis = fromColumns $ map (\j -> if j >= cols a then rows a |> e (j - cols a) else toColumns a !! j) jBasis
      aBasisInv = inv aBasis
  in if not $ all (\(j, x) -> j < cols a || x ?== 0) $ zip [0..] (toList xFirst)
     then FPSNothing
     else firstPhaseSimplex' a b c jBasis xFirst aBasis aBasisInv

firstPhaseSimplex' :: Matrix Double -> Vector Double -> Vector Double -> [Int] -> Vector Double -> Matrix Double -> Matrix Double -> FirstPhaseSimplexResult
firstPhaseSimplex' a b c jBasis xFirst aBasis aBasisInv =
  let jNonBasis = [x | x <- [0..(cols a - 1)], not $ Set.member x $ Set.fromList jBasis]
      aNonBasis = fromColumns $ map (toColumns a !!) jNonBasis
      ju = filter (>= cols a) jBasis
      jk = head ju
      k = fromJust $ elemIndex jk jBasis
      alpha = (toRows aBasisInv !! k) <> aNonBasis
      alphaNonZero = LA.find (?\= 0) alpha
  in if null ju
     then case simplex' a b c jBasis jNonBasis aBasisInv (fromList $ map (xFirst @>) jBasis) of
            SSolve x jBasis' -> FPSSolve x jBasis'
            SUnboundedObjectiveFunction -> FPSUnboundedObjectiveFunction
     else if not $ null alphaNonZero
     then let j0 = jNonBasis !! head alphaNonZero
              newJBasis = replaceSingle jk j0 jBasis
          in firstPhaseSimplex' a b c newJBasis xFirst aBasis aBasisInv
     else let aNew = fromRows $ removeByIndex (jk - cols a) $ toRows a
              bNew = fromList $ removeByIndex (jk - cols a) $ toList b
              aBasisNew = fromLists $ map (removeByIndex k) $ removeByIndex (jk - cols a) $ toLists aBasis
              aBasisInvNew = fromLists $ map (removeByIndex (jk - cols a)) $ removeByIndex k $ toLists aBasisInv
              newJBasis = removeByIndex k jBasis
          in firstPhaseSimplex' aNew bNew c newJBasis xFirst aBasisNew aBasisInvNew

simplex :: Matrix Double -> Vector Double -> Vector Double -> [Int] -> SimplexResult
simplex a b c jBasis =
  let aBasis = fromColumns $ map (toColumns a !!) jBasis
      aBasisInv = inv aBasis
      jNonBasis = [x | x <- [0..(cols a - 1)], not $ Set.member x $ Set.fromList jBasis]
      xBasis = aBasisInv <> b
  in simplex' a b c jBasis jNonBasis aBasisInv xBasis

simplex' :: Matrix Double -> Vector Double -> Vector Double -> [Int] -> [Int] -> Matrix Double -> Vector Double -> SimplexResult
simplex' a b c jBasis jNonBasis aBasisInv xBasis =
  let cBasis = fromList $ map (c @>) jBasis
      aNonBasis = fromColumns $ map (toColumns a !!) jNonBasis
      cNonBasis = fromList $ map (c @>) jNonBasis
      delta = cBasis <> aBasisInv <> aNonBasis - cNonBasis
      deltaLessZero = findIndices (?< 0) (toList delta)
      j0 = minimum $ map (jNonBasis !!) deltaLessZero
      z = aBasisInv <> (toColumns a !! j0)
      theta = zipWith (\x y -> if y ?<= 0 then Nothing else Just (x/y)) (toList xBasis) (toList z)
      thetaValues = catMaybes theta
      theta0 = minimum thetaValues
      js = minimum $ map (jBasis !!) $ elemIndices (Just theta0) theta
      s = fromJust $ elemIndex js jBasis
      newJBasis = replaceSingle js j0 jBasis
      newJNonBasis = replaceSingle j0 js jNonBasis
      newXBasis = mapVectorWithIndex (\ind x -> if ind == s then theta0 else x - theta0 * z @> ind) xBasis
      zs = z @> s
      z' = mapVectorWithIndex (\ind x -> if ind == s then 1/zs else -x/zs) z
      m = fromColumns $ map (\x -> if x == s then z' else rows a |> e x) [0..(rows a - 1)]
      newABasisInv = m <> aBasisInv
      xBasisByJ = Map.fromList $ zip jBasis (toList xBasis)
  in if null deltaLessZero
     then SSolve (fromList [Map.findWithDefault 0 j xBasisByJ | j <- [0..(cols a - 1)]]) jBasis
     else if null thetaValues
     then SUnboundedObjectiveFunction
     else simplex' a b c newJBasis newJNonBasis newABasisInv newXBasis

dualSimplex :: Matrix Double -> Vector Double -> Vector Double -> [Int] -> DualSimplexResult
dualSimplex a b c jBasis =
  let aBasis = fromColumns $ map (toColumns a !!) jBasis
      aBasisInv = inv aBasis
      cBasis = fromList $ map (c @>) jBasis
      jNonBasis = [x | x <- [0..(cols a - 1)], not $ Set.member x $ Set.fromList jBasis]
      aNonBasis = fromColumns $ map (toColumns a !!) jNonBasis
      cNonBasis = fromList $ map (c @>) jNonBasis
      delta = cBasis <> aBasisInv <> aNonBasis - cNonBasis
  in if all (?>= 0) (toList delta)
     then dualSimplex' a b c jBasis jNonBasis aBasisInv delta
     else DSInvalidBasis

dualSimplex' :: Matrix Double -> Vector Double -> Vector Double -> [Int] -> [Int] -> Matrix Double -> Vector Double -> DualSimplexResult
dualSimplex' a b c jBasis jNonBasis aBasisInv delta =
  let kappa = aBasisInv <> b
      kappaLessZero = LA.find (?< 0) kappa
      jk = minimum $ map (jBasis !!) kappaLessZero
      k = fromJust $ elemIndex jk jBasis
      mu = map ((toRows aBasisInv !! k <.>) . (toColumns a !!)) jNonBasis
      sigma = zipWith (\x y -> if y ?< 0 then Just (-x/y) else Nothing) (toList delta) mu
      sigmaValues = catMaybes sigma
      sigma0 = minimum sigmaValues
      j0 = minimum $ map (jNonBasis !!) $ elemIndices (Just sigma0) sigma
      newJBasis = replaceSingle jk j0 jBasis
      newJNonBasis = replaceSingle j0 jk jNonBasis
      newDelta = fromList $ map (\(x, y, j) -> if j == j0 then sigma0 else x + sigma0 * y) $ zip3 (toList delta) mu jNonBasis
      z = aBasisInv <> (toColumns a !! j0)
      zk = z @> k
      z' = mapVectorWithIndex (\ind x -> if ind == k then 1/zk else -x/zk) z
      m = fromColumns $ map (\x -> if x == k then z' else rows a |> e x) [0..(rows a - 1)]
      newABasisInv = m <> aBasisInv
      kappaByJ = Map.fromList $ zip jBasis (toList kappa)
  in if null kappaLessZero
     then DSSolve (fromList [Map.findWithDefault 0 j kappaByJ | j <- [0..(cols a - 1)]]) jBasis aBasisInv
     else if null sigmaValues
     then DSNothing
     else dualSimplex' a b c newJBasis newJNonBasis newABasisInv newDelta

dualSimplexMod :: Matrix Double -> Vector Double -> Vector Double -> Vector Double -> Vector Double -> [Int] -> DualSimplexModResult
dualSimplexMod a b c dMin dMax jBasis =
  let aBasis = fromColumns $ map (toColumns a !!) jBasis
      aBasisInv = inv aBasis
      cBasis = fromList $ map (c @>) jBasis
      jNonBasis = [j | j <- [0..(cols a - 1)], not $ Set.member j $ Set.fromList jBasis]
      delta = cBasis <> aBasisInv <> a - c
  in dualSimplexMod' a b c dMin dMax jBasis jNonBasis aBasisInv delta

dualSimplexMod' :: Matrix Double -> Vector Double -> Vector Double -> Vector Double -> Vector Double -> [Int] -> [Int] -> Matrix Double -> Vector Double -> DualSimplexModResult
dualSimplexMod' a b c dMin dMax jBasis jNonBasis aBasisInv delta =
  let kappa = aBasisInv <> (b - sum (map (\ j -> (toColumns a !! j) `mulVector` (if delta @> j ?>= 0 then dMin @> j else dMax @> j)) jNonBasis))
      kList = findIndices (\(x, dl, dr) -> x ?< dl || x ?> dr) $ zip3 (toList kappa) (map (dMin @>) jBasis) (map (dMax @>) jBasis)
      jk = minimum $ map (jBasis !!) kList
      k = fromJust $ elemIndex jk jBasis
      mujk = if kappa @> k ?< dMin @> jk then 1 else -1
      mu = map ((\ x -> mujk * (toRows aBasisInv !! k <.> x)) . (toColumns a !!)) jNonBasis
      --sigma = map (\(x, y) -> if (x ?>= 0 && y ?< 0) || (x ?< 0 && y ?> 0) then Just (-x/y) else Nothing) $ zip (map (delta @>) jNonBasis) mu
      sigma = zipWith (\j x -> if (dMin @> j ?== dMax @> j) || not ((delta @> j ?>= 0 && x ?< 0) || (delta @> j ?< 0 && x ?> 0))
                               then Nothing
                               else Just (-(delta @> j)/x)) jNonBasis mu
      sigmaValues = catMaybes sigma
      sigma0 = minimum sigmaValues
      j0 = minimum $ map (jNonBasis !!) $ elemIndices (Just sigma0) sigma
      newJBasis = replaceSingle jk j0 jBasis
      newJNonBasis = replaceSingle j0 jk jNonBasis
      muByJ = Map.fromList $ zip jNonBasis mu
      newDelta = fromList [if j == jk
                           then delta @> j + sigma0 * mujk
                           else if Map.member j muByJ
                           then delta @> j + sigma0 * muByJ Map.! j
                           else 0 | j <- [0..(cols a - 1)]]
      z = aBasisInv <> (toColumns a !! j0)
      zk = z @> k
      z' = mapVectorWithIndex (\ind x -> if ind == k then 1/zk else -x/zk) z
      m = fromColumns $ map (\x -> if x == k then z' else rows a |> e x) [0..(rows a - 1)]
      newABasisInv = m <> aBasisInv
      kappaByJ = Map.fromList $ zip jBasis (toList kappa)
  in if null kList
     then DSMSolve (fromList [Map.findWithDefault (if delta @> j ?>= 0 then dMin @> j else dMax @> j) j kappaByJ | j <- [0..(cols a - 1)]]) jBasis
     else if null sigmaValues then DSMNothing
     else dualSimplexMod' a b c dMin dMax newJBasis newJNonBasis newABasisInv newDelta
